<?php

add_shortcode('alwp_searchpage', 'of_alwp_searchpage');


function of_alwp_searchpage( $atts ){

if(!isset($_GET["book"]) || !isset($_GET["by"]) || $_GET["book"] == ""){

	return "<h2 style='padding:50px;text-align:center;'>Γεια σου, φαίνεται ότι δεν ψάξατε κανένα βιβλίο<br>Χρησιμοποιήστε την παραπάνω φόρμα για να το κάνετε</h2>";

}
else
{
	
	$host = get_option('of-alwp-libremip');
	$ping = ping($host, 3306, 2);
	if(!$ping) {
		return "<h2 style='padding:50px;text-align:center;'>Δυστυχώς ο κύριος διακομιστής δεν είναι προσβάσιμος αυτήν τη στιγμή<br>Δοκιμάστε ξανά αργότερα</h2>";
	}
	else
	{
		$con = mysqli_connect($host, "admin","qapl1209","librem");
		mysqli_set_charset($con,"utf8");
		
		$CONTENT = "";
		
		$sql="SELECT id,title,author,publisher,image FROM library WHERE  ". $_GET["by"] ." LIKE  '%". $_GET["book"] . "%' LIMIT 50";
		
		if ($result=mysqli_query($con,$sql))
		{
			while($row=mysqli_fetch_row($result)){
				
				if(empty($row[4])){
				$image = "/bookcovers/img.png";
				}
				else
				{
					$image = "data:image/png;base64," . $row[4];
				}
				$CONTENT .= "
					<div class='bookcov'><a href='./book/?bookid=". $row[0] ."'>
						<span class='cover' style='background-image: url($image);'></span>
						
						<span class='bc_title'>". $row[1]."</span>
						<span class='bc_author'>". $row[2]."</span>
						</a>
					</div>
				";
			}
		}
		
		return $CONTENT;
	}
}
}

?>