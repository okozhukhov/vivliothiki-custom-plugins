<?php
/**
* Plugin Name: ALWP
* Description: Wordpress plugins for alimos library.
* Version: 0.2.5
* Author: OuterFlow
* Author URI: http://outerflow.com
*/

//add the parent menu item if it doesnt already exist
if(!function_exists('of_main_menu_item')){
	add_action('admin_menu', 'of_main_menu_item');
	function of_main_menu_item() {
		add_menu_page('OuterFlow', 'OuterFlow', 'manage_options', 'of-main', 'of_main', plugins_url( 'of-ico.png', __FILE__ ) , 20);
		add_submenu_page( 'of-main', 'Outerflow', 'General Info', 'manage_options', 'of-main', 'of_main' );
	}
	function my_magic_function(){
		
	}
	function of_main(){
		echo file_get_contents("http://mtc.outerflow.com/motd/main.txt");
	}
}
//add the menu item for this plugin
add_action('admin_menu', 'of_alwp_menu');

function of_alwp_menu() {
	add_submenu_page( 'of-main', 'ALWP', 'ALWP', 'manage_options', 'of-alwp', 'of_alwp' );
	add_action( 'admin_init', 'of_alwp_settings' );
}
//Create the settings in wordpress
function of_alwp_settings() {
	register_setting( 'of_alwp_setting', 'of-alwp-libremip' );
}
//Load all needed files and styles for plugin
wp_enqueue_style( 'ofalwp-style', plugins_url( 'style.css', __FILE__ ) );
include(dirname(__FILE__) . '/of_library_booklist.php');
include(dirname(__FILE__) . '/of_library_search.php');
include(dirname(__FILE__) . '/of_library_searchpage.php');
include(dirname(__FILE__) . '/of_library_mybooks.php');
include(dirname(__FILE__) . '/of_library_book.php');

//Main admin section for ip/hostname setting
function of_alwp() {
?>
<div class="wrap">
<h2><b>A</b>limos <b>L</b>ibrary <b>W</b>ordpress <b>P</b>lugin</h2>
<form method="post" action="options.php">
<?php 
	settings_fields( 'of_alwp_setting' );
	do_settings_sections( 'of_alwp_setting' );
?>
<?php submit_button(); ?>
<table class="form-table">
		<tr valign="top">
        <th scope="row">Librem IP/Hostname<br><small>The IP or Hostname of the server containing the librem database</small></th>
		<td><input type="text" name="of-alwp-libremip" value="<?php echo esc_attr( get_option('of-alwp-libremip') ); ?>" /></td>
        </tr>
    </table>
	   <?php submit_button(); ?>
	</form>
</div>
<?php } ?>